import logging
import asyncio
from fastapi import FastAPI, Depends, WebSocket, HTTPException
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy import create_engine
from models.schemas import Host, CpuData, RxPkt, CpuDataCreate, RxPktCreate, CpuDataResponse, RxPktResponse, HostCreate, HostResponse
from sqlalchemy.exc import IntegrityError
from fastapi.middleware.cors import CORSMiddleware


from typing import List
from datetime import datetime

logging.basicConfig(level=logging.DEBUG)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



# Database Configuration
DATABASE_URL = "mysql+pymysql://root:42411218@localhost/metrics"
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# API Endpoints
@app.post("/add_host", response_model=HostResponse)
def add_host(host: HostCreate, db: Session = Depends(get_db)):
    existing_host = db.query(Host).filter(Host.hostname == host.hostname).first()

    if existing_host:
        print("Host already exists. Returning existing host ID:", existing_host.id)
        response_host = HostResponse(id=existing_host.id, hostname=existing_host.hostname)
        return response_host

    print("Received request to add host data")
    new_host = Host(**host.dict())
    try:
            db.add(new_host)
            db.commit()
            db.refresh(new_host)
            print("Host data added successfully")

            response_host = HostResponse(id=new_host.id, hostname=new_host.hostname)

            return response_host
    except IntegrityError:
            db.rollback()
            raise HTTPException(
                status_code=400, detail="Host with the same hostname already exists")
    

@app.post("/add_rx_pkt", response_model=RxPktResponse)
def add_rx_pkt(rx_pkt: RxPktCreate, db: Session = Depends(get_db)):
    print("Received request to add received packet data")
    existing_rx_pkt = db.query(RxPkt).filter(
        RxPkt.host_id == rx_pkt.host_id).first()

    if existing_rx_pkt:
        existing_rx_pkt.received_packets = rx_pkt.received_packets
        db.commit()
        db.refresh(existing_rx_pkt)
        print("Received packet data updated successfully")
        return existing_rx_pkt

    new_rx_pkt = RxPkt(**rx_pkt.dict(), timestamp=datetime.utcnow())
    db.add(new_rx_pkt)
    db.commit()
    db.refresh(new_rx_pkt)
    print("Received packet data added successfully")
    return new_rx_pkt

@app.get("/get_host_by_hostname/{hostname}", response_model=HostResponse)
def get_host_by_hostname(hostname: str, db: Session = Depends(get_db)):
    host = db.query(Host).filter(Host.hostname == hostname).first()
    if host is None:
        raise HTTPException(status_code=404, detail="Host not found")
    return host

@app.post("/add_cpu_data", response_model=CpuDataResponse)
def add_cpu_data(cpu_data: CpuDataCreate, db: Session = Depends(get_db)):
    print("Received request to add CPU data")
    existing_cpu_data = db.query(CpuData).filter(CpuData.host_id == cpu_data.host_id).order_by(CpuData.timestamp.desc()).first()

    if existing_cpu_data:
        existing_cpu_data.load_percent = cpu_data.load_percent
        db.commit()
        db.refresh(existing_cpu_data)
        print("CPU data updated successfully")
        return existing_cpu_data

    new_cpu_data = CpuData(**cpu_data.dict(), timestamp=datetime.utcnow())
    db.add(new_cpu_data)
    db.commit()
    db.refresh(new_cpu_data)
    print("CPU data added successfully")
    return new_cpu_data

@app.get("/get_cpu_data/{host_id}", response_model=List[CpuDataResponse])
def get_cpu_data(host_id: int, db: Session = Depends(get_db)):
    cpu_data = db.query(CpuData).filter(CpuData.host_id == host_id).all()
    return cpu_data  # Return a list of CpuData objects

@app.get("/get_rx_pkt/{host_id}", response_model=List[RxPktResponse])
def get_rx_pkt(host_id: int, db: Session = Depends(get_db)):
    rx_pkt = db.query(RxPkt).filter(RxPkt.host_id == host_id).all()
    return rx_pkt  # Return a list of RxPkt objects


@app.websocket("/ws/{host_id}")
async def websocket_endpoint(websocket: WebSocket, host_id: int):
    await websocket.accept()

    last_sent_cpu_load = None
    last_sent_received_packets = None

    while True:
        try:
            with SessionLocal() as db:
                latest_cpu_data = db.query(CpuData).filter(CpuData.host_id == host_id).order_by(CpuData.timestamp.desc()).first()
                latest_rx_pkt_data = db.query(RxPkt).filter(RxPkt.host_id == host_id).order_by(RxPkt.timestamp.desc()).first()

                if latest_cpu_data:
                    cpu_load_percent = latest_cpu_data.load_percent
                    if cpu_load_percent != last_sent_cpu_load:
                        print("CPU load percent changed. Sending data:", cpu_load_percent)
                        await websocket.send_json({"cpu_load_percent": cpu_load_percent})
                        last_sent_cpu_load = cpu_load_percent

                if latest_rx_pkt_data:
                    received_packets = latest_rx_pkt_data.received_packets
                    if received_packets != last_sent_received_packets:
                        print("Received packets changed. Sending data:", received_packets)
                        await websocket.send_json({"received_packets": received_packets})
                        last_sent_received_packets = received_packets

        except Exception as e:
            print("An error occurred:", e)

        await asyncio.sleep(1)

@app.get("/get_all_hosts", response_model=List[HostResponse])
def get_all_hosts(db: Session = Depends(get_db)):
    hosts = db.query(Host).all()
    return hosts

@app.get("/")
def read_root():
    return {"message": "Welcome to the Monitoring API"}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)  # Listen on all available network interfaces
