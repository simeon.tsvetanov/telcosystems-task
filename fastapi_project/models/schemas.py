from sqlalchemy import Column, Integer, String, Float, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from pydantic import BaseModel
from sqlalchemy.sql import func

Base = declarative_base()

class Host(Base):
    __tablename__ = "host"
    id = Column(Integer, primary_key=True, index=True)
    hostname = Column(String, unique=True, index=True)

    cpu_data = relationship("CpuData", back_populates="host")
    rx_pkt = relationship("RxPkt", back_populates="host")

class CpuData(Base):
    __tablename__ = "cpu_data"
    id = Column(Integer, primary_key=True, index=True)
    host_id = Column(Integer, ForeignKey("host.id"))
    load_percent = Column(Float)
    timestamp = Column(DateTime, server_default=func.now())
    host = relationship("Host", back_populates="cpu_data")

class RxPkt(Base):
    __tablename__ = "rx_pkt"
    id = Column(Integer, primary_key=True, index=True)
    host_id = Column(Integer, ForeignKey("host.id"))
    received_packets = Column(Integer)
    timestamp = Column(DateTime, server_default=func.now())
    host = relationship("Host", back_populates="rx_pkt")

class CpuDataCreate(BaseModel):
    host_id: int
    load_percent: float

class RxPktCreate(BaseModel):
    host_id: int
    received_packets: int

class HostCreate(BaseModel):
    hostname: str    

class CpuDataResponse(BaseModel):
    id: int
    host_id: int
    load_percent: float

class RxPktResponse(BaseModel):
    id: int
    host_id: int
    received_packets: int
    
class HostResponse(BaseModel):
    id: int
    hostname: str
