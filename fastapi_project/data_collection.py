import asyncio
import psutil
import requests
import time
import socket

# API URLs
HOST_URL = "http://localhost:8000"
CPU_DATA_URL = f"{HOST_URL}/add_cpu_data"
RX_PKT_URL = f"{HOST_URL}/add_rx_pkt"

# Get the active network interface name
active_interface = None
for interface, stats in psutil.net_if_stats().items():
    if stats.isup:
        active_interface = interface
        print("Active Network Interface:", active_interface)
        break


if not active_interface:
    print("No active network interface found.")
    exit()

# Fetch or create host ID
hostname = socket.gethostname()
response_host = requests.get(f"{HOST_URL}/get_host_by_hostname/{hostname}")

if response_host.status_code == 404:
    # Host not found, create a new one
    response_host = requests.post(f"{HOST_URL}/add_host", json={"hostname": hostname})
    if response_host.status_code == 200:
        host_id = response_host.json()["id"]
        print("New Host ID created:", host_id)
    else:
        print("Error creating new host:", response_host.text)
        exit()
elif response_host.status_code == 200:
    host_id = response_host.json()["id"]
    print("Existing Host ID retrieved:", host_id)
else:
    print("Error getting host:", response_host.text)
    exit()

# Function to collect and send data
def collect_and_send_data():
    last_sent_cpu_data = None
    last_sent_rx_pkt_data = None

    while True:
        try:
            # Collect data
            cpu_load_percent = psutil.cpu_percent(interval=1)
            rx_packets = psutil.net_io_counters().packets_recv
            
            # Send CPU data
            if cpu_load_percent != last_sent_cpu_data:
                cpu_data_payload = {"host_id": host_id, "load_percent": cpu_load_percent}
                response_cpu = requests.post(CPU_DATA_URL, json=cpu_data_payload)
                print("CPU Data Response Content:", response_cpu.text)
                last_sent_cpu_data = cpu_load_percent

            # Send received packets data
            if rx_packets != last_sent_rx_pkt_data:
                rx_pkt_payload = {"host_id": host_id, "received_packets": rx_packets}
                response_rx_pkt = requests.post(RX_PKT_URL, json=rx_pkt_payload)
                print("Received Packet Response Content:", response_rx_pkt.text)
                last_sent_rx_pkt_data = rx_packets

            time.sleep(1)  

        except Exception as e:
            print("An error occurred:", e)
            time.sleep(5)  

# Start data collection
collect_and_send_data()
