import React, { useState, useEffect } from "react";
import axios from "axios";
import DataDisplay from "./components/DataDisplay/DataDisplay";

const App = () => {
  const [hosts, setHosts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    // Fetch initial host data
    axios
      .get("http://localhost:8000/get_all_hosts")
      .then((response) => {
        setHosts(response.data);
        setIsLoading(false);
      })
      .catch((error) => console.error("Error fetching host data:", error));

  }, []);

  return (
    <div>
      <h1>Welcome to the Monitoring UI</h1>
      {isLoading ? (
        <p>Loading host data...</p>
      ) : (
        hosts.map((host) => (
          <DataDisplay key={host.id} host={host} />
        ))
      )}
    </div>
  );
};

export default App;
