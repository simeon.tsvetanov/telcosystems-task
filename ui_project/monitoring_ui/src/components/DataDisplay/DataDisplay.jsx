import React, { useState, useEffect } from "react";
import "./DataDisplay.css";

const DataDisplay = ({ host }) => {
  const [cpuLoad, setCpuLoad] = useState(null);
  const [receivedPackets, setReceivedPackets] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  // WebSocket connection setup
  useEffect(() => {
    const socket = new WebSocket(`ws://localhost:8000/ws/${host.id}`);

    socket.onopen = () => {
      setIsLoading(false);
    };

    socket.onmessage = (event) => {
      const data = JSON.parse(event.data);

      if (data.cpu_load_percent !== undefined) {
        setCpuLoad(data.cpu_load_percent);
      }

      if (data.received_packets !== undefined) {
        setReceivedPackets(data.received_packets);
      }
    };

    socket.onclose = (event) => {
      console.log("WebSocket connection closed", event);
    };

    return () => {
      if (socket.readyState === WebSocket.OPEN) {
        socket.close();
      }
    };
  }, [host.id]);

  const calculateCpuLoadColor = () => {
    if (cpuLoad === null) return "";

    if (cpuLoad > 90) {
      return "red";
    } else if (cpuLoad > 80) {
      return "orange";
    } else if (cpuLoad > 50) {
      return "yellow";
    }
    return "";
  };

  return (
    <div className="data-display">
      <div className="host-info">
        <h2 className="host-name">{host.hostname}</h2>
        <p className="host-id">Host ID: {host.id}</p>
      </div>
      {isLoading ? (
        <p>Loading data...</p>
      ) : (
        <div className="data">
          <div className={`cpu-load-bar ${calculateCpuLoadColor()}`}>
            <div
              className="cpu-load-fill"
              style={{ width: `${cpuLoad}%` }}
            ></div>
          </div>
          <p>
            CPU Load: {cpuLoad !== null ? `${cpuLoad.toFixed(2)}%` : "No data available"}
          </p>
          <p>
            Received Packets:{" "}
            {receivedPackets !== null ? receivedPackets : "No data available"}
          </p>
        </div>
      )}
    </div>
  );
};

export default DataDisplay;
